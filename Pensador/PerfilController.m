//
//  PerfilController.m
//  Pensador
//
//  Created by Andre Sousa on 19/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import "PerfilController.h"
#import "EditNameController.h"

@interface PerfilController () <EditNameDelegate>

@property (nonatomic, strong) UIImagePickerController *picker;

@end

@implementation PerfilController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.perfilButton.layer.cornerRadius = 4.0;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"username"]) {
        self.nameLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    }
}


#pragma mark -
#pragma mark Actions

- (IBAction)uploadPhoto:(id)sender
{
    [[[UIActionSheet alloc] initWithTitle:nil
                                delegate:self
                       cancelButtonTitle:@"Cancelar"
                  destructiveButtonTitle:nil
                        otherButtonTitles: @"Tirar uma foto", @"Escolher da galeria", nil]
     showInView:self.view];
}


#pragma mark -
#pragma mark Action Sheet Delegates

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [[[UIAlertView alloc] initWithTitle:@"Erro"
                                        message:@"Dispositivo não suporta esta funcionalidade."
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles: nil]
             show];
        } else {
            
            if (!self.picker) {
                self.picker = [[UIImagePickerController alloc] init];
                self.picker.delegate = self;
                self.picker.allowsEditing = YES;
                self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                self.picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            }
            
            [self presentViewController:self.picker animated:YES completion:nil];
        }
        
    } else if (buttonIndex == 1) {
        
        if (!self.picker) {
            self.picker = [[UIImagePickerController alloc] init];
            self.picker.delegate = self;
            self.picker.allowsEditing = YES;
            self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        
        [self presentViewController:self.picker animated:YES completion:nil];

    }
}


#pragma mark -
#pragma mark Image Picker Delegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.perfilImageView.layer.cornerRadius = floorf(self.perfilImageView.frame.size.width/2);
    self.perfilImageView.layer.masksToBounds = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.perfilImageView.image = chosenImage;
    });
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -
#pragma mark Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"editName"]) {
        [(EditNameController *)segue.destinationViewController setDelegate:self];
        if ([self.nameLabel.text isEqualToString:@"Nome"]) {
            [(EditNameController *)segue.destinationViewController setName:@""];
        } else {
            [(EditNameController *)segue.destinationViewController setName:self.nameLabel.text];
        }
    }
}


#pragma mark -
#pragma mark Edit Name Delegates

- (void)updateName:(NSString *)name
{
    self.nameLabel.text = name;
    
    [[NSUserDefaults standardUserDefaults] setObject:self.nameLabel.text forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
