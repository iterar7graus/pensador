//
//  main.m
//  Pensador
//
//  Created by Andre Sousa on 17/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
