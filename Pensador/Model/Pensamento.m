#import "Pensamento.h"

@interface Pensamento ()

// Private interface goes here.

@end

@implementation Pensamento

// Custom logic goes here.
+ (Pensamento *)pensamentoWithTitle:(NSString *)pensamentoTitle withManagedObjectContext:(NSManagedObjectContext *)moc
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[Pensamento entityName]];
    [request setPredicate:[NSPredicate predicateWithFormat:@"title == %@", pensamentoTitle]];
    [request setFetchLimit:1];
    
    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"ERROR: %@, %@", [error localizedDescription], [error userInfo]);
    }
    
    if ([results count] == 0) {
        return nil;
    }
    
    return [results objectAtIndex:0];
}

- (void)updateAttributes:(NSDictionary *)attributes
{
    self.title = [attributes objectForKeyOrNil:@"title"];
    self.sentenceDescription = [attributes objectForKeyOrNil:@"description"];
    self.link = [attributes objectForKeyOrNil:@"link"];
    self.author = [attributes objectForKeyOrNil:@"author"];
}

@end
