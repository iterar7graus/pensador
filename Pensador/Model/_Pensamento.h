// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Pensamento.h instead.

@import CoreData;

extern const struct PensamentoAttributes {
	__unsafe_unretained NSString *author;
	__unsafe_unretained NSString *link;
	__unsafe_unretained NSString *sentenceDescription;
	__unsafe_unretained NSString *title;
} PensamentoAttributes;

@interface PensamentoID : NSManagedObjectID {}
@end

@interface _Pensamento : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) PensamentoID* objectID;

@property (nonatomic, strong) NSString* author;

//- (BOOL)validateAuthor:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* link;

//- (BOOL)validateLink:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* sentenceDescription;

//- (BOOL)validateSentenceDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@end

@interface _Pensamento (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAuthor;
- (void)setPrimitiveAuthor:(NSString*)value;

- (NSString*)primitiveLink;
- (void)setPrimitiveLink:(NSString*)value;

- (NSString*)primitiveSentenceDescription;
- (void)setPrimitiveSentenceDescription:(NSString*)value;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

@end
