#import "_Pensamento.h"
#import "NSDictionary+ObjectForKeyOrNil.h"

@interface Pensamento : _Pensamento {}
// Custom logic goes here.
+ (Pensamento *)pensamentoWithTitle:(NSString *)pensamentoTitle withManagedObjectContext:(NSManagedObjectContext *)moc;

- (void)updateAttributes:(NSDictionary *)attributes;

@end
