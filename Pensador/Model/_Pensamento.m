// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Pensamento.m instead.

#import "_Pensamento.h"

const struct PensamentoAttributes PensamentoAttributes = {
	.author = @"author",
	.link = @"link",
	.sentenceDescription = @"sentenceDescription",
	.title = @"title",
};

@implementation PensamentoID
@end

@implementation _Pensamento

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Pensamento" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Pensamento";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Pensamento" inManagedObjectContext:moc_];
}

- (PensamentoID*)objectID {
	return (PensamentoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic author;

@dynamic link;

@dynamic sentenceDescription;

@dynamic title;

@end

