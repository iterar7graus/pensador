//
//  PensamentosViewController.m
//  Pensador
//
//  Created by Andre Sousa on 17/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import "PensamentosViewController.h"
#import "PensamentosCell.h"
#import "PensamentosSDK.h"
#import "PensamentosDetailViewController.h"
#import <CoreData/CoreData.h>
#import "Pensamento.h"
#import "CustomTransition.h"


@interface PensamentosViewController () <UIViewControllerTransitioningDelegate>

//@property (nonatomic, copy) NSArray *pensamentosArray;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

- (IBAction)refresh:(id)sender;

@end


@implementation PensamentosViewController


#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateTableView];
    [self fetchData];

    
}


#pragma mark -
#pragma mark Fetch data

- (void)fetchData
{
    [PensamentosSDK getNewThoughtsWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Reload table view
        
        [self updateTableView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorDescription) {
        // Deal with error
        dispatch_async(dispatch_get_main_queue(), ^{
            [[[UIAlertView alloc] initWithTitle:@"Erro"
                                        message:@"Não foi possível aceder aos mais recentes pensamentos"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles: nil] show];
        });
    }];
}

- (IBAction)refresh:(id)sender
{
    [self fetchData];
}

- (void)updateTableView
{
    _fetchedResultsController = [PensamentosSDK loadThoughts];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}


#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
    return [[_fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return self.pensamentosArray.count;
    return [[[_fetchedResultsController sections] objectAtIndex:0] numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"pensamentosCellIdentifier";
    
    PensamentosCell *cell = (PensamentosCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil) {
        cell = [[PensamentosCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
//    cell.titleLabel.text = [[self.pensamentosArray objectAtIndex:indexPath.row] objectForKey:@"title"];
//    cell.descriptionLabel.text = [[self.pensamentosArray objectAtIndex:indexPath.row] objectForKey:@"description"];
//    cell.authorLabel.text = [[self.pensamentosArray objectAtIndex:indexPath.row] objectForKey:@"author"];
    Pensamento *pensamento = [_fetchedResultsController objectAtIndexPath:indexPath];
    cell.titleLabel.text = pensamento.title;
    cell.descriptionLabel.text = pensamento.sentenceDescription;
    cell.authorLabel.text = pensamento.author;
    
    return cell;
}


#pragma mark - UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"showDetail" sender:indexPath];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showDetail"]) {
        NSIndexPath *index = (NSIndexPath *)sender;
        
//        NSString *titleString = [[self.pensamentosArray objectAtIndex:index.row] objectForKey:@"title"];
//        NSString *descriptionString = [[self.pensamentosArray objectAtIndex:index.row] objectForKey:@"description"];
//        NSString *authorName = [[self.pensamentosArray objectAtIndex:index.row] objectForKey:@"author"];
//        NSString *webURLString = [[self.pensamentosArray objectAtIndex:index.row] objectForKey:@"link"];
        
        Pensamento *pensamento = [_fetchedResultsController objectAtIndexPath:index];
        NSString *titleString = pensamento.title;
        NSString *descriptionString = pensamento.sentenceDescription;
        NSString *authorName = pensamento.author;
        NSString *webURLString = pensamento.link;
        
        
        [(PensamentosDetailViewController *)segue.destinationViewController setTitleString:titleString];
        [(PensamentosDetailViewController *)segue.destinationViewController setDescriptionString:descriptionString];
        [(PensamentosDetailViewController *)segue.destinationViewController setAuthorNameString:authorName];
        [(PensamentosDetailViewController *)segue.destinationViewController setWebLink:[NSURL URLWithString:webURLString]];
    }
}

//-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue
//{
//    
//}

@end
