//
//  PensamentosMapController.m
//  Pensador
//
//  Created by Andre Sousa on 18/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import "PensamentosMapController.h"

@interface PensamentosMapController ()

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) MKPointAnnotation *pin;

@end

@implementation PensamentosMapController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.locationManager) {
        self.locationManager = [CLLocationManager new];
        [self.locationManager requestWhenInUseAuthorization];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark -
#pragma mark Map Delegates

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (self.locationManager) {
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
        
        if (!self.pin) {
            self.pin = [MKPointAnnotation new];
            self.pin.coordinate = userLocation.coordinate;
            self.pin.title = @"Where am I?";
            
//            self.pin.subtitle = @"I'm here!";
            
            CLGeocoder *geoCoder = [CLGeocoder new];
            CLLocation *location = [[CLLocation alloc] initWithLatitude:userLocation.coordinate.latitude longitude:userLocation.coordinate.longitude];
            [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
                CLPlacemark *place;
                if (placemarks.count > 0) {
                    place = (CLPlacemark *)[placemarks objectAtIndex:0];
                    self.pin.subtitle = place.locality;
                }
                [self.mapView addAnnotation:self.pin];
            }];
        }
    }
}

@end
