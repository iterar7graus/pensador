//
//  PensamentosDetailViewController.m
//  Pensador
//
//  Created by Andre Sousa on 17/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import "PensamentosDetailViewController.h"
#import "PensamentosSDK.h"
#import "UIImageView+AFNetworking.h"
#import "PensamentosWebViewController.h"


@implementation PensamentosDetailViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = self.titleString;
    self.descriptionLabel.text = self.descriptionString;
    self.authorNameLabel.text = self.authorNameString;
    
    [self fetchAuthorImage];
}


#pragma mark -
#pragma mark Networking

- (void)fetchAuthorImage
{
    // New API
    [PensamentosSDK getNewImageOfAuthor:self.authorNameString
                            withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                                // Update image profile
                                if ([[responseObject objectForKey:@"count"] intValue] > 0) {
                                    NSString *urlString = [[[[[responseObject objectForKey:@"sources"] objectAtIndex:0] objectForKey:@"result"] objectAtIndex:0] objectForKey:@"url"];

                                    NSURL *imageURL = [NSURL URLWithString:urlString];
                                    [self.authorImageView setImageWithURL:imageURL placeholderImage:nil];
                                }
                            } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorDescription) {
                                // Do nothing
                            }];
}


#pragma mark -
#pragma mark Actions

- (IBAction)visitPage:(id)sender
{
    [[UIApplication sharedApplication] openURL:self.webLink];
}

- (IBAction)openWebView:(id)sender
{

}


#pragma mark -
#pragma mark Navigations

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showWebView"]) {
        
        [(PensamentosWebViewController *)[segue.destinationViewController topViewController] setUrlString:self.webLink.absoluteString];
    }
}


@end
