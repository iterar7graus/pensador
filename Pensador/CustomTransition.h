//
//  CustomTransition.h
//  Pensador
//
//  Created by Andre Sousa on 18/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTransition : UIStoryboardSegue

@end
