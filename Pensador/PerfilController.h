//
//  PerfilController.h
//  Pensador
//
//  Created by Andre Sousa on 19/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PerfilController : UIViewController <UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *perfilImageView;
@property (weak, nonatomic) IBOutlet UIButton *perfilButton;

- (IBAction)uploadPhoto:(id)sender;
- (void)updateName:(NSString *)name;

@end
