//
//  PensamentosWebViewController.m
//  Pensador
//
//  Created by Andre Sousa on 18/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import "PensamentosWebViewController.h"

@interface PensamentosWebViewController ()

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation PensamentosWebViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.webView.delegate = self;
    NSURL *url = [NSURL URLWithString:self.urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}


#pragma mark -
#pragma mark Webview delegates

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    if (!self.indicatorView) {
        self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        self.indicatorView.center = self.view.center;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.indicatorView];
//        [self.view addSubview:self.indicatorView];
    }
    self.indicatorView.color = [UIColor blueColor];
    [self.indicatorView startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [self.indicatorView stopAnimating];
        [self.indicatorView removeFromSuperview];
    });
    
    if ([self.webView canGoBack]) {
        UIImage *leftArrow = [UIImage imageNamed:@"arrow_back.png"];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:leftArrow
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(goBack)];
        
    } else {
        self.navigationItem.leftBarButtonItem = nil;
    }
}



#pragma mark -
#pragma mark Actions

- (IBAction)dismissWebView:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)goBack
{
    [self.webView goBack];
}

@end
