//
//  PensamentosWebViewController.h
//  Pensador
//
//  Created by Andre Sousa on 18/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PensamentosWebViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, copy) NSString *urlString;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
