//
//  PensamentosMapController.h
//  Pensador
//
//  Created by Andre Sousa on 18/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface PensamentosMapController : UIViewController <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
