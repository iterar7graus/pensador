//
//  EditNameController.h
//  Pensador
//
//  Created by Andre Sousa on 19/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EditNameDelegate <NSObject>
@optional
- (void)updateName:(NSString *)name;
@end

@interface EditNameController : UIViewController

@property (nonatomic) id<EditNameDelegate>delegate;

@property (nonatomic, copy) NSString *name;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@end
