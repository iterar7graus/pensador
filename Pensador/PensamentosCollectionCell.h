//
//  PensamentosCollectionCell.h
//  Pensador
//
//  Created by Tiago Alves on 18/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PensamentosCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
