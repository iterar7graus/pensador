//
//  NSDictionary+ObjectForKeyOrNil.h
//  Pensador
//
//  Created by Andre Sousa on 18/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (ObjectForKeyOrNil)

- (id)objectForKeyOrNil:(id)key;

@end
