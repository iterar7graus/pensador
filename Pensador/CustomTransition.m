//
//  CustomTransition.m
//  Pensador
//
//  Created by Andre Sousa on 18/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import "CustomTransition.h"

@implementation CustomTransition

- (void)perform
{
    UIViewController *srcViewController = self.sourceViewController;
    UIViewController *destViewController = self.destinationViewController;
    
    CGRect destFrame = destViewController.view.frame;
    destFrame.origin.y = - srcViewController.view.frame.size.height;
    destViewController.view.frame = destFrame;
    
    [[srcViewController.navigationController.view superview] addSubview:destViewController.view];
    [[srcViewController.navigationController.view superview] insertSubview:destViewController.view aboveSubview:srcViewController.view];
    
    CGRect finalDestFrame = destViewController.view.frame;
    finalDestFrame.origin.y = 0;
    
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         destViewController.view.frame = finalDestFrame;
                         
                     } completion:^(BOOL finished)
    {
        [srcViewController.navigationController presentViewController:destViewController animated:NO completion:nil];
    }];
}

@end
