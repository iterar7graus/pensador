//
//  EditNameController.m
//  Pensador
//
//  Created by Andre Sousa on 19/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import "EditNameController.h"

@implementation EditNameController


#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (![self.name isEqualToString:@""]) {
        self.nameTextField.text = self.name;
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Guardar" style:UIBarButtonItemStylePlain target:self action:@selector(saveName)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark -
#pragma mark Actions

- (void)saveName
{
    // Update name on previous view controller
    if ([self.delegate respondsToSelector:@selector(updateName:)]) {
        [self.delegate updateName:self.nameTextField.text];
    }
}

@end
