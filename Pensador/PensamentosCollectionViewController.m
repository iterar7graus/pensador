//
//  PensamentosCollectionViewController.m
//  Pensador
//
//  Created by Tiago Alves on 18/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import "PensamentosCollectionViewController.h"
#import "PensamentosSDK.h"
#import "Pensamento.h"
#import "PensamentosCollectionCell.h"
#import "UIImageView+AFNetworking.h"
#import "PensamentosDetailViewController.h"
#import <CoreData/CoreData.h>

@interface PensamentosCollectionViewController ()

@property (nonatomic, copy) NSArray *pensamentosArray;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation PensamentosCollectionViewController

static NSString * const reuseIdentifier = @"CollectionCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self updateCollectionView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [[_fetchedResultsController sections] count];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[[_fetchedResultsController sections] objectAtIndex:0] numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PensamentosCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    Pensamento *pensamento = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.imageView.image = nil;
    cell.tag = indexPath.row;
    [PensamentosSDK getNewImageOfAuthor:pensamento.author
                            withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                                // Update image profile
                                if ([[responseObject objectForKey:@"count"] intValue] > 0) {
                                    NSString *urlString = [[[[[responseObject objectForKey:@"sources"] objectAtIndex:0] objectForKey:@"result"] objectAtIndex:0] objectForKey:@"url"];
                                    
                                    NSURL *imageURL = [NSURL URLWithString:urlString];
                                    if (cell.tag == indexPath.row) {
                                        [cell.imageView setImageWithURL:imageURL placeholderImage:nil];
                                    }
                                }
                            } failure:^(AFHTTPRequestOperation *operation, NSError *error, NSString *errorDescription) {
                                // Do nothing
                            }];
    
    return cell;
}

- (void)updateCollectionView {
    _fetchedResultsController = [PensamentosSDK loadThoughts];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}

#pragma mark - UICollectionView Delegate methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"showDetail" sender:indexPath];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showDetail"]) {
        NSIndexPath *index = (NSIndexPath *)sender;
        
        Pensamento *pensamento = [_fetchedResultsController objectAtIndexPath:index];
        NSString *titleString = pensamento.title;
        NSString *descriptionString = pensamento.sentenceDescription;
        NSString *authorName = pensamento.author;
        NSString *webURLString = pensamento.link;
        
        
        [(PensamentosDetailViewController *)segue.destinationViewController setTitleString:titleString];
        [(PensamentosDetailViewController *)segue.destinationViewController setDescriptionString:descriptionString];
        [(PensamentosDetailViewController *)segue.destinationViewController setAuthorNameString:authorName];
        [(PensamentosDetailViewController *)segue.destinationViewController setWebLink:[NSURL URLWithString:webURLString]];
    }
}

@end
