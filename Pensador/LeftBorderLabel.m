//
//  LeftBorderLabel.m
//  Pensador
//
//  Created by Tiago Alves on 19/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import "LeftBorderLabel.h"

@implementation LeftBorderLabel

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, 5, 0, 5};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 2);
    CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
    
    CGContextMoveToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, 0, rect.size.height);
    
    CGContextStrokePath(context);
    
    [super drawRect:rect];
}

@end
