//
//  PensamentosSDK.h
//  Pensador
//
//  Created by Andre Sousa on 17/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import <CoreData/CoreData.h>

typedef void (^SuccessBlock)(AFHTTPRequestOperation *operation, id responseObject);
typedef void (^FailureBlock)(AFHTTPRequestOperation *operation, NSError *error, NSString* errorDescription);

@interface PensamentosSDK : NSObject

+ (void)getNewThoughtsWithSuccess:(SuccessBlock)success
                          failure:(FailureBlock)failure;

+ (void)getNewImageOfAuthor:(NSString *)authorName
                withSuccess:(SuccessBlock)success
                    failure:(FailureBlock)failure;

+ (NSFetchedResultsController *)loadThoughts;

@end
