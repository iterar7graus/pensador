//
//  PensamentosSDK.m
//  Pensador
//
//  Created by Andre Sousa on 17/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import "PensamentosSDK.h"
#import "DataModel.h"
#import "Pensamento.h"

#define kImagesAPIMainURL @"http://freeimages.pictures/api/user/5220433517824859/"

@implementation PensamentosSDK

+ (void)getNewThoughtsWithSuccess:(SuccessBlock)success
                          failure:(FailureBlock)failure
{
 
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:@"http://pensador.herokuapp.com/api/v1/sentences.json"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
//             NSLog(@"response: %@", responseObject);
             NSArray *thoughts = [responseObject copy];
             [self saveNewThoughts:thoughts];
             
             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

             success(operation, responseObject);
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

             failure(operation, error, @"Description");
         }];
}

+ (void)getNewImageOfAuthor:(NSString *)authorName
                withSuccess:(SuccessBlock)success
                    failure:(FailureBlock)failure
{

    NSString *urlString = [NSString stringWithFormat:@"%@?keyword=%@&format=json", kImagesAPIMainURL, authorName];
    NSString *noWhiteSpacesURLString = [[urlString stringByReplacingOccurrencesOfString:@" " withString:@"_"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:noWhiteSpacesURLString
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             success(operation, responseObject);
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             failure(operation, error, @"Description");
         }];

}

+ (void)saveNewThoughts:(NSArray *)thoughts
{
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [context setPersistentStoreCoordinator:[[DataModel sharedDataModel] persistentStoreCoordinator]];
    
    for (NSDictionary *objectDict in thoughts) {
        Pensamento *pensamento = [Pensamento pensamentoWithTitle:[objectDict objectForKey:@"title"] withManagedObjectContext:context];
        if (pensamento == nil) {
            pensamento = [Pensamento insertInManagedObjectContext:context];
            [pensamento setTitle:[objectDict objectForKey:@"title"]];
            [pensamento updateAttributes:objectDict];
        } else {
            [pensamento updateAttributes:objectDict];
        }
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"ERROR SAVING NEW THOUGHT: %@, %@", error.localizedDescription, error.userInfo);
    } else {
        NSLog(@"successfully saved or updated thoughts");
    }
}

+ (NSFetchedResultsController *)loadThoughts
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[Pensamento entityName]];
    NSSortDescriptor *authorSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"author" ascending:YES];
    NSSortDescriptor *titleSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
    [request setSortDescriptors:@[authorSortDescriptor, titleSortDescriptor]];
    
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                                               managedObjectContext:[[DataModel sharedDataModel] mainContext]
                                                                                                 sectionNameKeyPath:nil
                                                                                                          cacheName:nil];
    
    [fetchedResultsController performFetch:nil];
    return fetchedResultsController;
}

@end
