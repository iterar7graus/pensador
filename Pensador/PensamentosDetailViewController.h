//
//  PensamentosDetailViewController.h
//  Pensador
//
//  Created by Andre Sousa on 17/07/15.
//  Copyright (c) 2015 Iterar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftBorderLabel.h"

@interface PensamentosDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet LeftBorderLabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *authorImageView;
@property (weak, nonatomic) IBOutlet UILabel *authorNameLabel;
@property (nonatomic, copy) NSString *titleString;
@property (nonatomic, copy) NSString *descriptionString;
@property (nonatomic, copy) NSString *authorNameString;
@property (nonatomic, strong) NSURL *webLink;

- (IBAction)visitPage:(id)sender;
- (IBAction)openWebView:(id)sender;

@end
